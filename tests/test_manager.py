# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import pytest
from django.core.files.base import ContentFile

from fargo.fargo.models import Document

pytestmark = pytest.mark.django_db


def login(app, username='admin', password='admin', user=None):
    login_page = app.get('/login/')
    login_form = login_page.forms[0]
    if user:
        login_form['username'] = user.username
        login_form['password'] = user.username
    else:
        login_form['username'] = username
        login_form['password'] = password
    resp = login_form.submit()
    assert resp.status_int == 302
    return app


def test_document_delete(app):
    f = ContentFile(b'A test file, ez pz.', 'test_file.txt')
    doc = Document.objects.get_by_file(f)
    file_path = doc.content.path
    assert os.path.isfile(file_path)

    doc.delete()
    assert not os.path.isfile(file_path)
