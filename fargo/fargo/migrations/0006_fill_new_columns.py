from django.db import migrations


def fill_validation_user_and_content_hash(apps, schema_editor):
    for validation in apps.get_model('fargo', 'Validation').objects.select_related():
        validation.user = validation.user_document.user
        validation.content_hash = validation.user_document.document.content_hash
        validation.save()


def noop(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0005_auto_20160312_1809'),
    ]

    operations = [
        migrations.RunPython(fill_validation_user_and_content_hash, noop),
    ]
