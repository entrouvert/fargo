# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from django import forms
from django.conf import settings
from django.template.defaultfilters import filesizeformat
from django.utils.translation import gettext_lazy as _

from . import models


class UploadForm(forms.ModelForm):
    content = forms.FileField(label=_('file'), max_length=512)

    def clean_content(self):
        content = self.cleaned_data.get('content')
        if content:
            if content.size > settings.FARGO_MAX_DOCUMENT_SIZE:
                raise forms.ValidationError(
                    _('Uploaded file is too big (limit is %s)')
                    % filesizeformat(settings.FARGO_MAX_DOCUMENT_SIZE)
                )
            if settings.FARGO_FORBIDDEN_EXTENSIONS:
                ext = os.path.splitext(content.name)[-1]
                if ext in settings.FARGO_FORBIDDEN_EXTENSIONS:
                    raise forms.ValidationError(_('Uploaded file is not allowed.'))
        return content

    def clean(self):
        content = self.cleaned_data.get('content')
        if content:
            if (
                models.Document.objects.used_space(self.instance.user) + content.size
                > settings.FARGO_MAX_DOCUMENT_BOX_SIZE
            ):
                raise forms.ValidationError(
                    _('Your document box is full (limit is %s)') % settings.FARGO_MAX_DOCUMENT_BOX_SIZE
                )
        return self.cleaned_data

    def save(self, *args, **kwargs):
        self.instance.filename = self.files['content'].name[:512]
        self.instance.document = models.Document.objects.get_by_file(self.files['content'])
        return super().save(*args, **kwargs)

    class Meta:
        model = models.UserDocument
        fields = []


class EditForm(forms.ModelForm):
    class Meta:
        model = models.UserDocument
        fields = ['title', 'description', 'expiration_date']
        widgets = {'expiration_date': forms.TextInput(attrs={'type': 'date'})}
