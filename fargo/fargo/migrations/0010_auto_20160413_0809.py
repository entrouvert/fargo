from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0009_auto_20160326_2104'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userdocument',
            name='user',
            field=models.ForeignKey(
                related_name='user_documents',
                verbose_name='user',
                to=settings.AUTH_USER_MODEL,
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AlterField(
            model_name='validation',
            name='created',
            field=models.DateTimeField(verbose_name='creation date'),
        ),
    ]
