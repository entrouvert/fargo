from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='document',
            options={
                'ordering': ('-creation',),
                'verbose_name': 'document',
                'verbose_name_plural': 'documents',
            },
        ),
    ]
