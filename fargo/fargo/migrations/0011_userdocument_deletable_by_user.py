# Generated by Django 1.9.7 on 2016-06-29 14:37

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0010_auto_20160413_0809'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdocument',
            name='deletable_by_user',
            field=models.BooleanField(default=True, verbose_name='deletable by user'),
        ),
    ]
