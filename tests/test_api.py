# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64

import pytest
from test_manager import login

from fargo.fargo import models

pytestmark = pytest.mark.django_db


def test_push_document(app, admin_user, john_doe):
    login(app)
    data = {
        'user_email': john_doe.email,
    }
    url = '/api/documents/push/'
    # error checking test
    assert models.Origin.objects.count() == 0
    assert models.UserDocument.objects.count() == 0
    assert models.Document.objects.count() == 0
    response = app.post_json(url, params=data, status=400)
    assert models.Origin.objects.count() == 0
    assert models.UserDocument.objects.count() == 0
    assert models.Document.objects.count() == 0
    assert response.json['result'] == 0
    assert set(response.json['errors'].keys()) == {'origin', 'file_b64_content'}
    data.update(
        {
            'origin': 'wcs',
            'file_b64_content': base64.b64encode(b'coin').decode(),
            'file_name': 'monfichier.pdf',
        }
    )
    response = app.post_json(url, params=data, status=200)
    assert response.json['result'] == 1
    assert models.Origin.objects.count() == 1
    assert models.Origin.objects.get().label == 'wcs'
    assert models.Origin.objects.get().slug == 'wcs'
    assert models.UserDocument.objects.count() == 1
    assert models.UserDocument.objects.get().user == john_doe
    assert models.UserDocument.objects.get().deletable_by_user is True
    assert models.Document.objects.count() == 1
    assert models.Document.objects.get().content.read() == b'coin'
    assert models.UserDocument.objects.get().document == models.Document.objects.get()
    assert models.UserDocument.objects.get().origin == models.Origin.objects.get()

    data['file_b64_content'] = base64.b64encode(b'coin2').decode()
    data['deletable_by_user'] = False
    response = app.post_json(url, params=data, status=200)
    assert response.json['result'] == 1
    assert models.Origin.objects.count() == 1  # same origin
    assert models.UserDocument.objects.count() == 2  # new document
    assert models.UserDocument.objects.filter(deletable_by_user=False).count() == 1

    data['file_b64_content'] = base64.b64encode(b'coin3').decode()
    data['deletable_by_user'] = True
    response = app.post_json(url, params=data, status=200)
    assert response.json['result'] == 1
    assert models.UserDocument.objects.count() == 3  # new document
    assert models.UserDocument.objects.filter(deletable_by_user=True).count() == 2

    # same document
    data['file_b64_content'] = base64.b64encode(b'coin3').decode()
    data['deletable_by_user'] = True
    response = app.post_json(url, params=data, status=400)
    assert response.json['result'] == 0
    assert response.json['errors'][0]['code'] == 'document-exists'
    assert models.UserDocument.objects.count() == 3  # no new document


def test_push_document_max_document_size(app, private_settings, admin_user, john_doe):
    private_settings.FARGO_MAX_DOCUMENT_SIZE = 1
    login(app)
    url = '/api/documents/push/'
    data = {
        'user_email': john_doe.email,
        'origin': 'wcs',
        'file_b64_content': base64.b64encode(b'coin').decode(),
        'file_name': 'monfichier.pdf',
    }
    response = app.post_json(url, params=data, status=400)
    assert response.json['result'] == 0
    assert list(response.json['errors'].keys()) == ['file_b64_content']
    assert response.json['errors']['file_b64_content'][0]['code'] == 'too-big'
    assert response.json['errors']['file_b64_content'][0]['limit'] == '1'


def test_push_document_max_document_box_size(app, private_settings, admin_user, john_doe):
    private_settings.FARGO_MAX_DOCUMENT_BOX_SIZE = 4
    login(app)
    url = '/api/documents/push/'
    data = {
        'user_email': john_doe.email,
        'origin': 'wcs',
        'file_b64_content': base64.b64encode(b'coin').decode(),
        'file_name': 'monfichier.pdf',
    }
    response = app.post_json(url, params=data, status=200)
    assert response.json['result'] == 1
    assert models.Document.objects.count() == 1
    response = app.post_json(url, params=data, status=400)
    assert response.json['result'] == 0
    assert list(response.json['errors'].keys()) == ['__all__']
    assert response.json['errors']['__all__'][0]['code'] == 'box-is-full'
    assert response.json['errors']['__all__'][0]['limit'] == '4'


def test_push_document_slashed_name(app, admin_user, john_doe):
    login(app)
    url = '/api/documents/push/'
    data = {
        'user_email': john_doe.email,
        'origin': 'wcs',
        'file_b64_content': base64.b64encode(b'whatever').decode(),
        'file_name': 'monfichier 18/06/2017.pdf',
    }
    response = app.post_json(url, params=data, status=200)
    assert response.json['result'] == 1
    assert models.Document.objects.count() == 1
    doc = models.UserDocument.objects.first()
    assert doc.filename == 'monfichier 18-06-2017.pdf'
    assert doc.get_download_url() == '/%s/download/monfichier%%252018-06-2017.pdf' % doc.pk
    login(app, user=john_doe)
    app.get(doc.get_download_url(), status=200)


def test_push_document_long_filename(app, admin_user, john_doe):
    login(app)
    url = '/api/documents/push/'
    data = {
        'user_email': john_doe.email,
        'origin': 'wcs',
        'file_b64_content': base64.b64encode(b'whatever').decode(),
        'file_name': '%s.pdf' % ('abc' * 100),
    }
    response = app.post_json(url, params=data, status=200)
    assert response.json['result'] == 1
    assert models.Document.objects.count() == 1
    doc = models.UserDocument.objects.first()
    assert doc.filename.startswith('abc') and doc.filename.endswith('.pdf')
    assert doc.get_download_url().endswith('.pdf')
    login(app, user=john_doe)
    app.get(doc.get_download_url(), status=200)
