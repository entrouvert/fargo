from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('document_filename', models.CharField(max_length=512, verbose_name='document filename')),
                ('document_file', models.FileField(upload_to=b'', verbose_name='file')),
                ('creation', models.DateTimeField(auto_now_add=True, verbose_name='creation date')),
                (
                    'user',
                    models.ForeignKey(
                        verbose_name='user', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
    ]
