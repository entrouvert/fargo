# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path

from .fargo.api_views import push_document, recent_documents
from .fargo.views import (
    delete,
    download,
    edit,
    home,
    json,
    jsonp,
    login,
    logout,
    pick,
    pick_list,
    remote_download,
    thumbnail,
    upload,
)

urlpatterns = [
    path('', home, name='home'),
    path('pick/', pick_list, name='list_to_pick'),
    path('jsonp/', jsonp, name='jsonp'),
    path('json/', json, name='json'),
    path('<int:pk>/edit/', edit, name='edit'),
    path('<int:pk>/delete/', delete, name='delete'),
    path('<int:pk>/pick/', pick, name='pick'),
    re_path(r'^(?P<pk>\d+)/download/(?P<filename>[^/]*)$', download, name='download'),
    re_path(r'^(?P<pk>\d+)/thumbnail/(?P<filename>[^/]*)$', thumbnail, name='thumbnail'),
    path('upload/', upload, name='upload'),
    re_path(r'^remote-download/(?P<filename>[^/]*)$', remote_download, name='remote_download'),
    re_path(r'^admin/', admin.site.urls),
    path('login/', login, name='auth_login'),
    path('logout/', logout, name='auth_logout'),
    path('api/documents/push/', push_document, name='fargo-api-push-document'),
    path('api/documents/recently-added/', recent_documents),
]

if settings.DEBUG and 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns.append(path('accounts/mellon/', include('mellon.urls')))
