from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('fargo', '0004_auto_20160212_0936'),
    ]

    operations = [
        migrations.AddField(
            model_name='validation',
            name='content_hash',
            field=models.CharField(max_length=128, null=True, verbose_name='content hash', blank=True),
        ),
        migrations.AddField(
            model_name='validation',
            name='user',
            field=models.ForeignKey(
                verbose_name='user', to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE
            ),
        ),
    ]
