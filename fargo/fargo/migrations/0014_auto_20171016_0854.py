from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0013_document_mime_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdocument',
            name='description',
            field=models.TextField(verbose_name='description', blank=True),
        ),
        migrations.AddField(
            model_name='userdocument',
            name='expiration_date',
            field=models.DateField(null=True, verbose_name='expiration date', blank=True),
        ),
        migrations.AddField(
            model_name='userdocument',
            name='title',
            field=models.CharField(max_length=200, verbose_name='title', blank=True),
        ),
    ]
