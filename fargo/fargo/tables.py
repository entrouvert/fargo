# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django_tables2 as tables
from django.utils.translation import gettext_lazy as _

from . import models


class DocumentTable(tables.Table):
    '''Display the list of documents of the user'''

    size = tables.TemplateColumn(
        template_code='{{ record.document.content.size|filesizeformat }}',
        orderable=False,
        verbose_name=_('Size'),
    )
    created = tables.DateTimeColumn(verbose_name=_('Creation Date'))

    class Meta:
        model = models.UserDocument
        fields = ('size', 'created')
        empty_text = _('Your document box is currently empty.')
