from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0012_auto_20161124_0626'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='mime_type',
            field=models.CharField(max_length=256, blank=True),
        ),
    ]
