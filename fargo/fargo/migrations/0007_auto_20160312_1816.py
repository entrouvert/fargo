from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0006_fill_new_columns'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='validation',
            name='user_document',
        ),
        migrations.AlterField(
            model_name='validation',
            name='user',
            field=models.ForeignKey(
                verbose_name='user', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
            ),
        ),
    ]
