from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0007_auto_20160312_1816'),
    ]

    operations = [
        migrations.AddField(
            model_name='validation',
            name='origin',
            field=models.ForeignKey(
                verbose_name='origin', to='fargo.Origin', null=True, on_delete=models.CASCADE
            ),
        ),
    ]
