# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from django.core.management import call_command

from fargo.fargo.models import Document, UserDocument


def test_cleanup(freezer, john_doe):
    start = freezer()

    foo = Document.objects.create(content=ContentFile(b'foo', name='foo.txt'))
    UserDocument.objects.create(user=john_doe, document=foo, filename='foo.txt', title='', description='')

    call_command('fargo-cleanup')

    assert UserDocument.objects.all().count()
    assert Document.objects.all().count() == 1

    User.objects.all().delete()

    assert not UserDocument.objects.all().count()
    assert Document.objects.all().count() == 1

    call_command('fargo-cleanup')

    assert Document.objects.all().count() == 1

    freezer.move_to(start + datetime.timedelta(days=3))

    call_command('fargo-cleanup')

    assert not Document.objects.count()
