from pathlib import Path

import nox

nox.options.reuse_venv = True


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def setup_venv(session, *packages, django_version, drf_version):
    packages = [
        f'django{django_version}',
        f'djangorestframework{drf_version}',
        'django-filter>=2.4,<23.2',
        'django-tables2==2.4.1',
        '-r',
        'test-requirements.txt',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    session.install('-e', '.', *packages, silent=False)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


@nox.session
@nox.parametrize(
    'django,drf', [('>=3.2,<3.3', '>=3.4,<3.15'), ('>=4.2,<4.3', '>=3.13,<3.15')], ids=('django3', 'django4')
)
def tests(session, django, drf):
    setup_venv(session, django_version=django, drf_version=drf)

    session.run('python', 'manage.py', 'compilemessages', silent=True)

    args = ['py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            '--junitxml=junit-django.xml',
            '--cov',
            '--cov-append',
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--cov-context=test',
            '--cov-config',
            '.coveragerc',
        ]

    args += session.posargs

    if not session.interactive:
        args += ['-v']

    hookable_run(
        session,
        *args,
        env={
            'DJANGO_SETTINGS_MODULE': 'fargo.settings',
            'FARGO_SETTINGS_FILE': 'tests/settings.py',
        },
    )


@nox.session
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))
