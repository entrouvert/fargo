# Generated by Django 2.2.19 on 2021-08-24 07:57

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('fargo', '0001_squashed_0017_auto_20180331_1532'),
    ]

    operations = [
        migrations.AlterField(
            model_name='origin',
            name='label',
            field=models.TextField(verbose_name='Label'),
        ),
        migrations.AlterField(
            model_name='origin',
            name='slug',
            field=models.SlugField(max_length=256, verbose_name='Slug'),
        ),
    ]
