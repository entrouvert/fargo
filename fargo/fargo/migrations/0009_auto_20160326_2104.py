from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0008_validation_origin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='content',
            field=models.FileField(upload_to=b'uploads/', verbose_name='file'),
        ),
    ]
