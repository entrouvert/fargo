# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management import call_command
from django.core.management.base import BaseCommand

from fargo.fargo.utils import cleanup


class Command(BaseCommand):
    help = 'Clean expired models'

    def add_arguments(self, parser):
        # set verbosity=0 by default, because solr-thumbnail is too talkative
        # with verbosity=1, and cleanup-fargo is targeted for cron.
        parser.set_defaults(verbosity=0)

    def handle(self, *args, **options):
        cleanup()
        verbosity = int(options.get('verbosity'))
        call_command('thumbnail', 'cleanup', verbosity=verbosity)
