# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import urllib.parse

import pytest
from django.test import override_settings
from django.urls import reverse
from webtest import Upload

try:
    import magic
except ImportError:
    magic = None

from test_manager import login

from fargo.fargo.models import UserDocument

pytestmark = pytest.mark.django_db


def test_unlogged(app):
    assert urllib.parse.urlparse(app.get('/', status=302).location).path == '/login/'


def test_upload(app, john_doe):
    login(app, user=john_doe)
    response1 = app.get('/')
    form = response1.form
    form['content'] = Upload('monfichier.pdf', b'coin', 'application/pdf')
    response2 = form.submit()
    assert response2['Location'] in ['/', 'http://testserver/']
    response2 = response2.follow()
    assert 'monfichier.pdf' in response2.text
    if magic is not None:
        assert UserDocument.objects.get(filename='monfichier.pdf').document.mime_type == 'text/plain'
        assert ' mime-text ' in response2.text
        assert ' mime-text-plain' in response2.text

    UserDocument.objects.all().delete()

    response1 = app.get('/')
    form = response1.form
    form['content'] = Upload('monfichier.pdf', b'%PDF-1.4 ...', 'application/pdf')
    response2 = form.submit().follow()
    assert 'monfichier.pdf' in response2.text
    assert '12 bytes' in response2.text
    if magic is not None:
        assert UserDocument.objects.get(filename='monfichier.pdf').document.mime_type == 'application/pdf'
        assert ' mime-application ' in response2.text
        assert ' mime-application-pdf' in response2.text


def test_upload_max_size(app, private_settings, john_doe):
    private_settings.FARGO_MAX_DOCUMENT_SIZE = 1
    login(app, user=john_doe)
    response1 = app.get('/')
    form = response1.form
    form['content'] = Upload('monfichier.pdf', b'coin', 'application/pdf')
    response2 = form.submit()
    assert response2.status_code == 200
    assert 'Uploaded file is too big' in response2.text


def test_upload_max_document_box_size(app, private_settings, john_doe):
    private_settings.FARGO_MAX_DOCUMENT_BOX_SIZE = 4
    login(app, user=john_doe)
    response1 = app.get('/')
    form = response1.form
    form['content'] = Upload('monfichier.pdf', b'coin', 'application/pdf')
    response2 = form.submit().follow()
    assert 'monfichier.pdf' in response2.text
    response1 = app.get('/')
    form = response1.forms['send-file']
    form['content'] = Upload('monfichier.pdf', b'coin', 'application/pdf')
    response2 = form.submit()
    assert response2.status_code == 200
    assert 'Your document box is full (limit is 4)' in response2.text


def test_pick(app, private_settings, john_doe, user_doc):
    login(app, user=john_doe)

    return_url = 'http://client.org/callback/'
    response = app.get(reverse('list_to_pick') + '?pick=' + return_url)
    response = response.forms[0].submit('Pick')
    assert response['Location'].startswith(return_url)
    assert '?url=' in response['Location']


@override_settings(KNOWN_SERVICES={'wcs': {'forms': {'url': 'http://example.org/'}}})
def test_pick_url_check(app, private_settings, john_doe, user_doc):
    login(app, user=john_doe)
    resp = app.get('/')
    resp.form['content'] = Upload('monfichier.txt', b'coin', 'text/plain')
    resp = resp.form.submit().follow()
    pk = UserDocument.objects.all().first().id

    app.get(reverse('list_to_pick') + '?pick=http://client.org/callback/', status=403)
    app.get(reverse('list_to_pick') + '?pick=http://example.org/callback/', status=200)
    app.post(reverse('pick', kwargs={'pk': pk}) + '?pick=http://client.org/callback/', status=403)
    app.post(reverse('pick', kwargs={'pk': pk}) + '?pick=http://example.org/callback/', status=302)


def test_delete(app, john_doe, jane_doe):
    login(app, user=john_doe)
    resp = app.get('/')
    resp.form['content'] = Upload('monfichier.txt', b'coin', 'text/plain')
    resp = resp.form.submit().follow()
    assert 'monfichier.txt' in resp.text
    assert UserDocument.objects.all().count() == 1
    resp = resp.click(href=r'.*delete/')
    resp = resp.form.submit().follow()
    resp = app.get('/')
    assert 'monfichier.txt' not in resp.text
    assert UserDocument.objects.all().count() == 0

    # put it back
    resp.form['content'] = Upload('monfichier.txt', b'coin', 'text/plain')
    resp = resp.form.submit().follow()
    assert 'monfichier.txt' in resp.text
    assert UserDocument.objects.all().count() == 1
    resp = resp.click(href=r'.*delete/')
    delete_url = resp.request.url

    # login as another user
    login(app, user=jane_doe)
    resp = app.get('/')
    assert 'monfichier.txt' not in resp.text
    resp = app.get(delete_url, status=404)


def test_max_documents_per_user(app, private_settings, john_doe):
    private_settings.FARGO_MAX_DOCUMENTS_PER_USER = 1
    login(app, user=john_doe)
    response1 = app.get('/')
    assert len(response1.forms) == 2
    form = response1.form
    form['content'] = Upload('monfichier.pdf', b'coin', 'application/pdf')
    response2 = form.submit().follow()
    assert 'monfichier.pdf' in response2.text
    assert len(response2.forms) == 0

    response2 = form.submit().follow()
    assert 'monfichier.pdf' in response2.text
    assert len(response2.forms) == 0

    assert UserDocument.objects.count() == 1

    response = app.get('/upload/')
    assert response.location == '/'


def test_forbidden_extension(app, private_settings, john_doe):
    private_settings.FARGO_FORBIDDEN_EXTENSIONS = ['.txt']
    login(app, user=john_doe)
    resp = app.get('/')
    resp.form['content'] = Upload('monfichier.pdf', b'coin', 'application/pdf')
    resp = resp.form.submit().follow()
    assert UserDocument.objects.count() == 1

    resp = app.get('/')
    resp.form['content'] = Upload('monfichier.txt', b'coin', 'text/plain')
    resp = resp.form.submit()
    assert 'Uploaded file is not allowed.' in resp.text
    assert UserDocument.objects.count() == 1
