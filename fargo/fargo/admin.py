# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from django.utils.translation import gettext_lazy as _
from django.views.decorators.cache import never_cache

from . import models


@never_cache
def login(request, *args, **kwargs):
    try:
        auth_login_url = reverse('auth_login')
    except NoReverseMatch:
        return admin.site.orig_login(request, *args, **kwargs)
    auth_login_url += '?%s' % request.GET.urlencode()
    return redirect(auth_login_url)


@never_cache
def logout(request, *args, **kwargs):
    try:
        return redirect(reverse('auth_logout'))
    except NoReverseMatch:
        return admin.site.orig_logout(request, *args, **kwargs)


if admin.site.login != login:
    admin.site.orig_login = admin.site.login
    admin.site.login = login

if admin.site.logout != logout:
    admin.site.orig_logout = admin.site.logout
    admin.site.logout = logout


class UserDocumentAdmin(admin.ModelAdmin):
    list_display = ['user', 'filename', 'thumbnail', 'created', 'origin']
    fields = ['id', 'user', 'filename', 'thumbnail', 'created', 'origin']
    readonly_fields = ['created', 'thumbnail']
    search_fields = [
        'user__first_name',
        'user__last_name',
        'user__email',
        'filename',
        'origin__label',
        'document__content_hash',
    ]

    @admin.display(description=_('thumbnail'))
    def thumbnail(self, instance):
        return instance.document.thumbnail_img_tag


class DocumentAdmin(admin.ModelAdmin):
    fields = ['content_hash', 'thumbnail', 'users']
    list_display = ['content_hash', 'thumbnail', 'users']
    readonly_fields = ['thumbnail', 'users']
    search_fields = ['content_hash', 'content']

    def users(self, instance):
        User = get_user_model()
        qs = User.objects.filter(user_documents__document=instance)
        return ', '.join('%s' % u for u in qs)

    @admin.display(description=_('thumbnail'))
    def thumbnail(self, instance):
        return instance.thumbnail_img_tag


class OriginAdmin(admin.ModelAdmin):
    fields = ['label', 'slug']
    list_display = ['label', 'slug']


admin.site.register(models.UserDocument, UserDocumentAdmin)
admin.site.register(models.Document, DocumentAdmin)
admin.site.register(models.Origin, OriginAdmin)
