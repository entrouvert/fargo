# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import logging

import django_webtest
import pytest
from django.contrib.auth.models import User
from django.core.files.base import ContentFile

from fargo.fargo.models import Document, UserDocument


class WebTestMixin(django_webtest.WebTestMixin):
    csrf_checks = False


@pytest.fixture(autouse=True)
def media(settings, tmpdir):
    settings.MEDIA_ROOT = str(tmpdir.mkdir('media'))
    return settings.MEDIA_ROOT


@pytest.fixture
def app(request):
    wtm = WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp()


@pytest.fixture
def concurrency(settings):
    """Select a level of concurrency based on the db, sqlite3 is less robust
    thant postgres due to its transaction lock timeout of 5 seconds.
    """
    if 'sqlite' in settings.DATABASES['default']['ENGINE']:
        return 20
    else:
        return 100


@pytest.fixture
def private_settings(request):
    import django.conf
    from django.conf import UserSettingsHolder

    old = django.conf.settings._wrapped
    django.conf.settings._wrapped = UserSettingsHolder(old)

    def finalizer():
        django.conf.settings._wrapped = old

    request.addfinalizer(finalizer)
    return django.conf.settings


@pytest.fixture
def caplog(caplog):
    import py.io

    caplog.setLevel(logging.INFO)
    caplog.handler.stream = py.io.TextIO()
    caplog.handler.records = []
    return caplog


@pytest.fixture
def john_doe(db):
    user = User(username='john.doe', first_name='John', last_name='Doe', email='john.doe@example.com')
    user.set_password('john.doe')
    user.save()
    return user


@pytest.fixture
def jane_doe(db):
    user = User(username='jane.doe', first_name='Jane', last_name='Doe', email='jane.doe@example.com')
    user.set_password('jane.doe')
    user.save()
    return user


@pytest.fixture
def admin_user(db):
    try:
        user = User.objects.get(username='admin')
    except User.DoesNotExist:
        user = User.objects.create_superuser('admin', email=None, password='admin')
    return user


@pytest.fixture
def document():
    with open('tests/test_file.txt', 'rb') as f:
        content = ContentFile(f.read(), 'test_file.txt')

    return Document.objects.get_by_file(content)


@pytest.fixture
def user_doc(document, john_doe):
    return UserDocument.objects.create(user=john_doe, document=document, filename='éléphant.txt')
