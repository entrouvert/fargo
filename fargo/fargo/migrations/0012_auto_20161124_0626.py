from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0011_userdocument_deletable_by_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='content',
            field=models.FileField(upload_to=b'uploads/', max_length=300, verbose_name='file'),
        ),
    ]
