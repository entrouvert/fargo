$(function() {
  if (window.parent) {
    $('.buttons').hide();
  }
  $(window).on('message', function(ev) {
    var data = JSON.parse(ev.originalEvent.data);
    if (data.message == 'helloFargo') {
      window.parent.postMessage(JSON.stringify({
              'message': 'fargoButtons',
              'buttons': [
                  {'text': $('button[name=cancel]').text(),
                   'name': 'cancel'
                  },
                  {'text': $('button[name=submit]').text(),
                   'name': 'submit'
                  }
              ]}), '*');
    }
    if (data.message == 'validate') {
       $('button[name=submit]').click();
    }
  });
});
