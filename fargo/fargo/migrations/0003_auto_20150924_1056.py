import datetime

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import migrations, models
from django.utils.timezone import utc


def clear_document(apps, schema_editor):
    Document = apps.get_model('fargo', 'Document')
    Document.objects.all().delete()


def noop(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('fargo', '0002_auto_20150818_2117'),
    ]

    operations = [
        migrations.RunPython(clear_document, noop),
        migrations.CreateModel(
            name='UserDocument',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('filename', models.CharField(max_length=512, verbose_name='filename')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='creation date')),
            ],
            options={
                'ordering': ('-created', 'user'),
                'verbose_name': 'user document',
                'verbose_name_plural': 'user documents',
            },
        ),
        migrations.CreateModel(
            name='Validation',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('document_type', models.CharField(max_length=256, verbose_name='document type')),
                ('data', JSONField(null=True, verbose_name='data', default=dict)),
                ('start', models.DateField(verbose_name='start date')),
                ('end', models.DateField(verbose_name='end date')),
                ('creator', models.CharField(max_length=256, verbose_name='creator')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='creation date')),
                (
                    'user_document',
                    models.ForeignKey(
                        verbose_name='user document', to='fargo.UserDocument', on_delete=models.CASCADE
                    ),
                ),
            ],
        ),
        migrations.AlterModelOptions(
            name='document',
            options={
                'ordering': ('content_hash',),
                'verbose_name': 'document',
                'verbose_name_plural': 'documents',
            },
        ),
        migrations.RenameField(
            model_name='document',
            old_name='document_file',
            new_name='content',
        ),
        migrations.RemoveField(
            model_name='document',
            name='creation',
        ),
        migrations.RemoveField(
            model_name='document',
            name='document_filename',
        ),
        migrations.RemoveField(
            model_name='document',
            name='id',
        ),
        migrations.RemoveField(
            model_name='document',
            name='user',
        ),
        migrations.AddField(
            model_name='document',
            name='content_hash',
            field=models.CharField(
                default=datetime.datetime(2015, 9, 24, 10, 56, 54, 873399, tzinfo=utc),
                max_length=128,
                serialize=False,
                verbose_name='content hash',
                primary_key=True,
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userdocument',
            name='document',
            field=models.ForeignKey(
                related_name='user_documents',
                verbose_name='document',
                to='fargo.Document',
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AddField(
            model_name='userdocument',
            name='user',
            field=models.ForeignKey(
                verbose_name='user', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
            ),
        ),
        migrations.RunPython(noop, clear_document),
    ]
