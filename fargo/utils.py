# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib.parse

from django.utils.http import urlencode


def make_url(__url, **kwargs):
    request = kwargs.pop('request', None)
    parsed = urllib.parse.urlparse(__url)
    query = urllib.parse.parse_qs(parsed.query)
    for key, value in kwargs.items():
        if value is not None:
            query[key] = value
    parsed = parsed[:4] + (urlencode(query),) + parsed[5:]
    url = urllib.parse.urlunparse(parsed)
    if request:
        return request.build_absolute_uri(url)
    return url
