from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fargo', '0003_auto_20150924_1056'),
    ]

    operations = [
        migrations.CreateModel(
            name='Origin',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('label', models.CharField(max_length=80, verbose_name='Label')),
                ('slug', models.SlugField(verbose_name='Slug')),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='userdocument',
            name='origin',
            field=models.ForeignKey(
                verbose_name='origin', to='fargo.Origin', null=True, on_delete=models.CASCADE
            ),
            preserve_default=True,
        ),
    ]
