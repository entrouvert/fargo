# fargo - document box
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.translation import gettext_lazy as _
from rest_framework.serializers import ValidationError


class APIError(ValidationError):
    MISSING_USER = {'code': 'missing-user', 'msg': _('an user must be specified')}
    UNKNOWN_USER = {'code': 'unknown-user', 'msg': _('user is unknown')}
    TOO_MANY_USERS = {'code': 'too-many-users', 'msg': _('too many users have this email')}
    NOT_BASE64 = {'code': 'not-base64', 'msg': _('data is not base64 encoded')}
    NOT_STRING = {'code': 'not-string', 'msg': _('data is not a string')}
    TOO_BIG = {'code': 'too-big', 'msg': _('file is too big (limit is {limit})')}
    BOX_IS_FULL = {'code': 'box-is-full', 'msg': _('box is full (limit is {limit})')}
    DOCUMENT_EXISTS = {'code': 'document-exists', 'msg': _('user already have this document')}

    def __init__(self, kind, **kwargs):
        assert hasattr(self, kind), 'error %s is not defined' % kind

        detail = getattr(self, kind).copy()
        detail.update(kwargs)
        detail['msg'] = detail['msg'].format(**kwargs)
        super().__init__([detail])
